<?php
define('CSS_FILENAME', 'ez.css');

// implementation of hook_init()
function ezcss_init() {
  global $theme_key;
  // if we're currently in the "admin theme" and the admin theme doesn't match the default theme, then don't load ez.css
  $dontload = (bool)($theme_key == variable_get('admin_theme') && variable_get('theme_default', '') != variable_get('admin_theme', ''));
  if (!$dontload && file_exists(drupal_realpath('public://').'/'.CSS_FILENAME)) {
    $options = array();
    $options['group'] = CSS_THEME;    // need to put at theme layer since we're 
                                      // really trying to mimic a theme file
    $options['every_page'] = TRUE;
    $options['weight'] = 100;         // should be sufficiently high?
    $options['preprocess'] = FALSE;   // don't aggregate so changes to this file don't need a cache flush
    drupal_add_css(variable_get('file_public_path', conf_path() . '/files') .'/'.CSS_FILENAME, $options);
  }
}

// implementation of hook_permission
function ezcss_permission() {
  return array(
    'use ezcss' => array(
      'title' => t('Use E-Z CSS'),
      'description' => t('Can interact with and save the styles'),
    ),
  );
}

// implementation of hook_menu
function ezcss_menu() {
  $items = array();
  
  $items['admin/config/development/ezcss'] = array(
    'title' => 'E-Z CSS',
    'description' => 'Modify a site-wide CSS stylesheet on-site.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ezcss_edit_form'),
    'access arguments' => array('use ezcss'),
  );
  
  return $items;
}

// forms API for admin/config/ezcss
function ezcss_edit_form($form, &$form_state) {
  // Load CodeMirror library if libraries is installed
  if (module_exists('libraries')) {
    $codemirror_path = libraries_get_path('codemirror');
    if (!empty($codemirror_path) && file_exists($codemirror_path . '/lib/codemirror.js')) {
      drupal_add_css($codemirror_path . '/lib/codemirror.css');
      drupal_add_js($codemirror_path . '/lib/codemirror.js');
      drupal_add_css($codemirror_path . '/mode/css/css.css');
      drupal_add_js($codemirror_path . '/mode/css/css.js');
      // NOTE: we've gotta scope this to footer to make sure it runs after anything
      // else that might mess with that textarea (perhaps textarea.js, etc)
      $options = array();
      $options['type'] = 'inline';
      $options['scope'] = 'footer';
      drupal_add_js('
                    var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("edit-css"),
                      {
                        lineNumbers:true,
                        matchBrackets:true
                      });
                    check_syntax();
                    ', $options);
      drupal_add_css('.grippie { display:none }', $options);  // scope footer not valid for css but should be ignored
    }
  }
  
  drupal_add_css(drupal_get_path('module', 'ezcss').'/ezcss.css');
  drupal_add_js(drupal_get_path('module', 'ezcss').'/ezcss.js');
  
  $results = '';
  if (!empty($form_state['values']['validate_button'])) {
    $results = _ezcss_jigsaw_call($form_state['values']['css']);
  }

  global $base_url;
  $form['imce'] = array(
    '#markup' => '<div id="ezcss-tools"><a href="'.$base_url.'/imce" class="popup">File Browser</a></div>',
  );
  
  $contents = file_get_contents(drupal_realpath('public://').'/'.CSS_FILENAME);
  $form['css'] = array(
    '#type' => 'textarea',
    '#rows' => '18',
    '#default_value' => $contents,
  );  

  $form['validation_response'] = array(
    '#prefix' => '<div id="ezcss-validation">',
    '#markup' =>  $results,
    '#suffix' => '</div>',
  );
  
  $form['validate_button'] = array(
    '#type' => 'button',
    '#value' => 'Validate',
    '#ajax' => array(
      'callback' => 'ezcss_validator_callback',
      'wrapper' => 'ezcss-validation',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  
  return $form;
}

// SUBMIT handler for the ezcss save button
function ezcss_edit_form_submit($form, &$form_state) {
  $handle = fopen(drupal_realpath('public://').'/'.CSS_FILENAME, 'w');
  
  // @TODO - will likely need to sanitize line endings
  fwrite($handle, $form_state['values']['css']);
  fclose($handle);
  drupal_set_message('File was saved.');
} 

// validate the css form
function ezcss_validator_callback($form, $form_state) {  
  return $form['validation_response'];
}

function _ezcss_jigsaw_call($css) {
    // create a new cURL resource
  $ch = curl_init();

  // set URL and other appropriate options
  curl_setopt($ch, CURLOPT_URL, "http://jigsaw.w3.org/css-validator/validator?output=text&text=".urlencode($css));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  
  // grab URL and pass it to the browser
  $results = curl_exec($ch);
  $add_class = '';
  if (strpos($results, 'Sorry!') > 0) {
    $add_class = ' error';
  }
  $results = '<div class="jigsaw-results'.$add_class.'">'.check_markup($results).'</div>';
  // close cURL resource, and free up system resources
  curl_close($ch);
  return $results;
}

