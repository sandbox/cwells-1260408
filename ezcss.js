function check_syntax() {
  myCodeMirror.on("changes", function() {
    myCodeMirror.save();
  });
}

Drupal.behaviors.ezcss = {
  attach: function() {
    jQuery('a.popup').click(function(e) {
      var href = jQuery(this).attr('href');
      newwindow = window.open(href, 'imce', 'height=500,width=650');
      if (window.focus) { newwindow.focus(); }
      e.preventDefault();
      return false;
    });
  }
}
